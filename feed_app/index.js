const express = require("express");
require("./src/db/mongoose");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./feed_swagger.json");
const filterRouter = require("./src/routers/filter");

dotenv.config();

const app = express();
const server = require("http").createServer(app);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());
app.use(filterRouter);

console.log(
  "Open http://localhost:3000/api-docs to explore swagger and make requests"
);


const port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log("Server is up on port " + port);
});
module.exports = server;
