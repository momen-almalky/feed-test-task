const chai = require("chai");
const expect = chai.expect;
const filter_sample = require('../samples/filter_sample');
const request = require('supertest');

describe('filter router', () => {
    before(() => { 
        server = require('../../index');
        database = require('../../src/db/mongoose');
    });

    describe('POST /filters', () => {
        it('should create filter in database', async () => {    
          const res = await request(server).post('/filters')
          .send(filter_sample);
          expect(res.status).eql(200);
        });
    });

    describe('GET /filters', () => {
        it('should list filters in database', async () => {    
          const res = await request(server).get('/filters?name=Marketing Feed');
          expect(res.status).eql(200);
        });
    });
});
