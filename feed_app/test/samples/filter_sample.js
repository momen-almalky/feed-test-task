module.exports = {
  name: "Marketing Feed",
  filters: {
    gender: "male",
    language: "ar",
    dialect: "egy",
    followers_count_range: {
      gte: 10,
      lte: 90,
    },
  },
};
