const express = require("express");
const Filter = require("../models/filter");
const router = new express.Router();
const {
  newFilterValidation,
  getFilterValidation,
} = require("../validations/newFilterValidation");
const axios = require("axios").default;
require("dotenv").config();

/**
 * Create New Filter
 * @param req
 * @param res
 */
router.post("/filters", async (req, res) => {
  try {
    const { body } = req;

    const { error } = newFilterValidation(body);

    if (error) {

    console.log(error.details[0].message);

      return res.status(422).send({ message: error.details[0].message });
    }

    if (await Filter.findOne({ name: body.name })) {
      return res.status(400).send({ message: "this Filter name used before" });
    }

    const filter = new Filter({ ...body });
    await filter.save();

    res.status(200).send({ filter: filter });
  } catch (err) {
    console.log(err);
    res.status(400).send({ err: err });
  }
});

/**
 * Get Filter by name
 * @param req
 * @param res
 */
router.get("/filters", async (req, res) => {
  try {
    const { query } = req;

    const { error } = getFilterValidation(query);
    if (error) {
      return res.status(422).send({ message: error.details[0].message });
    }

    const filter = await Filter.findOne({ name: query.name });

    if (!filter) {
      return res.status(404).send({ message: "filter not found" });
    }

    const params = {
      filters: JSON.stringify(filter.filters)
    }
    const search = await axios.get(
      `${process.env.ES_SERVICE_URL}/elastic/feeds/search`,
      {
        params: params,
      }
    );

    if (search.status != 200) {
      res.status(400).send({ err: 'could not perform search query'  });
    }
    
    res.status(200).send({ data: search.data.search });
  } catch (err) {
    console.log(err);
    res.status(400).send({ err: err });
  }
});

module.exports = router;
