const mongoose = require("mongoose");
const constants = require("../helpers/constants");


var filterSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      min: [1, "Too short, min is 5 characters"],
      max: [50, "Too long, max is 32 characters"],
      required: true,
    },
    filters: {
      gender: {
        type: String,
        enum: constants.gender_list,
      },
      dialect: {
        type: String,
        enum: constants.dialect_list,
      },
      language: {
        type: String,
        enum: constants.language_list,
      },
      followers_count_range: {
        gte: {
          type: Number,
        },
        lte: {
          type: Number,
        },
      },
    },
  },
  { timestamps: true }
);

filterSchema.pre("save", async function (next) {
  const now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  this.updatedAt = now;
  next();
});

filterSchema.methods.toJSON = function () {
  const filter = this;
  const filterObject = filter.toObject();

  if (filterObject.filters.language == 'en') {
    delete filterObject.filters.dialect;
  }
  
  return filterObject;
};

const Filter = mongoose.model("Filter", filterSchema);
module.exports = Filter;
