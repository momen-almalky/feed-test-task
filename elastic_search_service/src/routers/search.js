const express = require("express");
const router = new express.Router();
const { searchFeedValidation } = require("../validations/searchFeedValidation");
const {
  searchFeedsQuery, createBody
} = require("../elasticSearch/schemas/searchFeedSchema");
const ESIndexManager = require("../elasticSearch/esIndexManager");

/**
 * Search Feeds by filters
 * @param req
 * @param res
 */
router.get("/elastic/feeds/search", async (req, res) => {
  try {
    const { query } = req;

    const filters = JSON.parse(query.filters);
    const { error } = searchFeedValidation(filters);

    if (error) {
      return res.status(422).send({ message: error.details[0].message });
    }

    let esIndexManager = new ESIndexManager(
      process.env.ES_INDEX,
      process.env.ES_DOCUMENT_TYPE
    );
    

    const search_query = searchFeedsQuery(filters);

    const search = await esIndexManager.search(search_query);
    
    res.status(200).send({ search: search });
  } catch (err) {
    console.log(err);
    res.status(400).send({ err: err });
  }
});

module.exports = router;
