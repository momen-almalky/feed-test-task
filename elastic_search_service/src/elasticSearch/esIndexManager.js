const elasticsearch = require("@elastic/elasticsearch");
require("dotenv").config();
const util = require("util");
const feed_json = require("../json_files/feedSample.json");
const mappingFeedSchema = require("../elasticSearch/schemas/mappingFeedSchema");
const elasticUrl = process.env.ES_URL || "http://localhost:9200";
const client = new elasticsearch.Client({ node: elasticUrl });

class EsIndexManager {
  /**
   * EsIndexManager Constructor
   * @param index_name {String}
   * @param type {String}
   */
  constructor(index_name, type) {
    this.index_name = index_name || `feed_index`;
    this.type = type || `feeds`;
  }

  /**
   * Check Connection
   *
   */
  checkConnection() {
    return new Promise(async (resolve) => {
      console.log("Checking connection to ElasticSearch...");
      let isConnected = false;

      while (!isConnected) {
        try {
          await client.cluster.health({});
          console.log("Successfully connected to ElasticSearch");
          isConnected = true;
        } catch (_) {}
      }

      resolve(true);
    });
  }

  /**
   * Create Index
   *
   */
  async createIndex() {
    try {
      return await client.indices.create({
        index: this.index_name,
      });
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }

  /**
   * Create Index
   *
   */
  async intialize() {
    try {
      const isElasticReady = await this.checkConnection();
      if (isElasticReady) {
        const elasticIndex = await this.indexExists();
        if (!elasticIndex.body) {
          await this.createIndex();
        }
        await this.createMapping(mappingFeedSchema.properties);

        await this.indexJsonObjects(feed_json);
        return true;
      }
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }

  /**
  /**
   * Create Mapping schema to help with query
   * @param schema {Object}
   */
  async createMapping(schema) {
    try {
      return await client.indices.putMapping({
        index: this.index_name,
        type: this.type,
        includeTypeName: true,
        body: { schema },
      });
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }

  /**
   * Check if indexExist
   *
   */
  async indexExists() {
    try {
      return await client.indices.exists({
        index: this.index_name,
      });
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }

  /**
   * index json objects into elastic search
   * @param file_name {string}
   */
  async indexJsonObjects(documents) {
    try {
      for (let doc of documents) {
        let id = doc.id;
        delete doc.id;
        await this.addDocument(id, this.type, doc);
      }
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }

  /**
   * Import Json Data from file
   * @param id {string}
   * @param docType {string}
   * @param payload {object}
   */
  async addDocument(id, docType, payload) {
    try {
      await client.index({
        index: this.index_name,
        type: docType,
        id: id,
        body: payload,
      });
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }

  /**
   * Search on ElasticSearch by query
   * @param body {object}
   */
  async search(search_query) {
    try {

      const response = await client.search({
        from: 0,
        size: 100,
        index: this.index_name,
        type: this.type,
        body: search_query,
      });

      const results = response.body.hits.total.value;
      const values = response.body.hits.hits.map((hit) => {
        return {
          id: hit._id,
          text: hit._source.text,
          user_gender: hit._source.user_gender,
          lang: hit._source.lang,
          dialect: hit._source.dialect,
          user: hit._source.user,
        };
      });

      return {
        results,
        values,
      };
    } catch (error) {
      console.log(util.inspect(error, false, null, true));
    }
  }
}

module.exports = EsIndexManager;
