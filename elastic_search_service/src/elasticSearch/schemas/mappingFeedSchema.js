module.exports.feed_schema = {
    properties: {
      text: { type: "text" },
      user_gender: { type: "keyword" },
      lang: { type: "keyword" },
      dialect: { type: "keyword" },
      user: {
        type: "nested",
        properties: {
          id: { type: "keyword" },
          followers_count: { type: "integer" },
        },
      },
    },

};
