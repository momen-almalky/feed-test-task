let bodybuilder = require("bodybuilder");

module.exports.searchFeedsQuery = function (body) {
  return {
    query: {
      bool: {
        must: [
          body.gender && {
            terms: {
              user_gender: [body.gender],
            },
          },
          body.language && {
            terms: {
              lang: [body.language],
            },
          },
          body.dialect && {
            terms: {
              dialect: [body.dialect],
            },
          },
          body.followers_count_range && {
            range: {
              "user.followers_count": {
                gte: body.followers_count_range.gte,
                lte: body.followers_count_range.lte,
              },
            },
          },
        ],
      },
    },
  };
};

module.exports.createBody = function (body) {
  let body_builder = bodybuilder();
  if (body.gender) {
    body_builder.query("term", "user_gender", body.gender);
  }

  if (body.language) {
    body_builder.query("term", "lang", body.language);
  }

  if (body.dialect) {
    body_builder.query("term", "dialect", body.dialect);
  }

  if (body.followers_count_range) {
    body_builder.query("range", "userfollowers_count", {
      gte: body.followers_count_range.gte,
      lte: body.followers_count_range.lte,
    });
  }
  
  body_builder.build();

  return body_builder;
};
