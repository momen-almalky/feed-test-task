const Joi = require("joi");
const constants = require("../helpers/constants");

const searchFeedValidation = (data) => {
  const schema =Joi.object().min(1).required()
      .keys({
        gender: Joi.string().min(1).valid(...constants.gender_list),
        dialect: Joi.string().min(1).valid(...constants.dialect_list),
        language: Joi.string().min(1).valid(...constants.language_list),
        followers_count_range: Joi.object().min(1)
          .keys({
            gte: Joi.number().min(0),
            lte: Joi.number().min(0),
          }),
  });

  return schema.validate(data, { allowUnknown:true });
};

module.exports.searchFeedValidation = searchFeedValidation;
