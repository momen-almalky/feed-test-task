const express = require("express");
const bodyParser = require("body-parser");
const dotenv = require("dotenv");
const ESIndexManager = require("./src/elasticSearch/esIndexManager");
const search_router = require("./src/routers/search");

dotenv.config();

const app = express();
const server = require("http").createServer(app);

app.use(bodyParser.json());
app.use(search_router);

let esIndexManager = new ESIndexManager(
  process.env.ES_INDEX,
  process.env.ES_DOCUMENT_TYPE
);

esIndexManager.intialize();

const port = process.env.PORT || 4000;
server.listen(port, () => {
  console.log("ElasticSearch is up on port " + port);
});
module.exports = server;
