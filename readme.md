# Feed Test Task

This is a test task that implement feed searching filters. When the user select a filter,apply this filter on elasticsearch data and return the matched conversations.

## Outline

 - [Libraries](#Libraries)
 - [Installation](#installation)
 - [Dependencies](#Dependencies)
 - [Swagger](#Swagger)
 - [Architecture](#Architecture)

## Libraries

This project is using several libraries and frameworks:

 - [@elastic/elasticsearch](https://www.npmjs.com/package/@elastic/elasticsearch) - ElasticSearch Client
 - [joi](https://www.npmjs.com/package/joi) - Joi Validator
 - [axios](https://www.npmjs.com/package/axios) - Axios
 - [mongoose](https://www.npmjs.com/package/mongoose) - Database

## Installation

### Dependencies

Make sure you have Docker:

```bash
$ docker-compose build
```

Then type into the terminal:

```bash
$ docker-compose up
```

Verify you see 
 Server is up on port 3000
 ElasticSearch is up on port 4000
"Open http://localhost:3000/api-docs to explore swagger and make requests"



## Swagger

This App is using swagger as a documentation for the APIs you can open swagger on following link
http://localhost:3000/api-docs

## Architecture

The idea was to create a service (node) that handle searches on elasticsearch separated from the 
node that handle user requests.

The architecture of the app are based on two nodes:

### Feed App (NODE)

This app handle the request to save and retrieve feeds and responsible to handle MongoDB request (handle the request from and to user).

### ElasticSearch Service (NODE)


This service handle the build of queries and searching on the elasticsearch.


### Request Methods

As a rule of thumb, the request methods is as follows:

|Method|Description|link|
| ------ | ------ | ----- |
|get|For Search by filter name|http://localhost:3000/filters|
|post|For Create Filter|http://localhost:3000/filters|
|get|For use search on elasticsearch node|http://localhost:4000/elastic/feeds/search|


### Confession
Due to meet the deadline I couldn't investigate whats wrong with these:
- The elastic search doesn't work good with the filters like for example if two of filters is deleted 
it will not work and it will crash.
- I was plan to use the body-builder on the elasticsearch queries.